package com.luojuan.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class RedisLock {
    @Autowired
    RedisTemplate redisTemplate;

    public String acquireLock(String lockName,long acquireTimeout,long lockTimeout){
        String identifier = UUID.randomUUID().toString();
        String lockkey = "lock:"+lockName;
        int lockExpire = (int) (lockTimeout/1000);
        long end = System.currentTimeMillis()+acquireTimeout;
        while(System.currentTimeMillis()<end){
           boolean lock =  redisTemplate.opsForValue().setIfAbsent(lockkey,identifier,lockExpire, TimeUnit.SECONDS);
            if(lock){
                return identifier;
            }
            if(redisTemplate.getExpire(lockkey)==-1){
                redisTemplate.expire(lockkey,lockExpire,TimeUnit.SECONDS);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
