package com.luojuan.lock.impl;

import com.luojuan.lock.RedisLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisLockImpl implements RedisLock {
    @Autowired
    RedisTemplate redisTemplate;
    private ThreadLocal<String> threadLocal = new ThreadLocal<>();

    @Override
    public boolean tryLock(String key, String value, int expire, TimeUnit timeUnit) {
        boolean lock = false;
        if(threadLocal.get()==null) {
            while(true){
                lock = redisTemplate.opsForValue().setIfAbsent(key, value, expire, timeUnit);
                if(lock){
                    threadLocal.set(value);
                    break;
                }
            }
        }else{
            if(threadLocal.get().equals(value)) {
                long restTime = redisTemplate.getExpire(key);
                if((restTime-System.currentTimeMillis())<expire){
                    redisTemplate.expire(key,expire,TimeUnit.SECONDS);
                }
                lock = true;
            }
        }

        return lock;
    }

    @Override
    public void releaseLock(String key) {
        if(threadLocal.get().equals(redisTemplate.opsForValue().get(key))){
            redisTemplate.delete(key);
        }
    }
}
