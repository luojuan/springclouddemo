package com.luojuan.lock;

import java.util.concurrent.TimeUnit;

public interface RedisLock {
    public boolean tryLock(String key, String value, int expire, TimeUnit timeUnit);

    public void releaseLock(String key);
}
