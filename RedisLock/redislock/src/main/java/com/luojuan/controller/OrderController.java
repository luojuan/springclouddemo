package com.luojuan.controller;

import com.luojuan.lock.RedisLock;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisLock redisLock;
    @Autowired
    Redisson redisson;

    @GetMapping("/buy")
    public String buy(){
//        boolean lock = redisLock.tryLock("buyKey",String.valueOf(Thread.currentThread().getId()),10, TimeUnit.SECONDS);
//        if(!lock){
//            return "error";
//        }
        RLock rLock = redisson.getLock("buyKey");
        rLock.lock();
        try {
            int stock = Integer.parseInt(stringRedisTemplate.opsForValue().get("stock"));
            if (stock > 0) {
                int realStock = stock - 1;
                System.out.println("扣减库存" + realStock);
                stringRedisTemplate.opsForValue().set("stock", realStock + "");
            } else {
                System.out.println("扣减失败 库存不足");
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            rLock.unlock();
//            redisLock.releaseLock("buyKey");
        }

        return "end";
    }

}
