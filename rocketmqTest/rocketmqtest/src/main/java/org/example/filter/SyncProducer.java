package org.example.filter;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;

/**
 * Hello world!
 *
 */
public class SyncProducer
{
    public static void main( String[] args ) throws MQClientException, UnsupportedEncodingException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("producer_group1");
        producer.setNamesrvAddr("192.168.231.128:9876");
        producer.setInstanceName("producer");
        //失败重试次数
        producer.setRetryTimesWhenSendAsyncFailed(0);
        producer.start();
        //设置消息条件
        for(int i=0;i<3;i++){
            Message message = new Message("TopicTest","TagA",("hello: "+i).getBytes(RemotingHelper.DEFAULT_CHARSET));
            message.putUserProperty("a",""+i);
            if(i%2==0){
                message.putUserProperty("b","yangguo");
            }else{
                message.putUserProperty("b","xiaolong nv");
            }
            SendResult sendResult = producer.send(message);
        }
        producer.shutdown();
    }
}
