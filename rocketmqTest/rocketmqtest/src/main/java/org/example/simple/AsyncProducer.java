package org.example.simple;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class AsyncProducer {
    public static void main(String[] args) throws MQClientException, UnsupportedEncodingException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("producer_group1");
        producer.setNamesrvAddr("192.168.231.128:9876");
        producer.setInstanceName("producer");
        producer.start();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        //失败重试次数
        producer.setRetryTimesWhenSendAsyncFailed(0);
        //异步推送消息，消息成功与失败会马上通过回调函数返回
        Message message = new Message("TopicTest","TagB","hello".getBytes(RemotingHelper.DEFAULT_CHARSET));
        producer.send(message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                countDownLatch.countDown();
                System.out.println("发送成功"+sendResult.getMsgId());
            }

            @Override
            public void onException(Throwable throwable) {
                countDownLatch.countDown();
                System.out.println("发送失败"+throwable.getMessage());
            }
        });
        countDownLatch.await(5, TimeUnit.SECONDS);
        producer.shutdown();
    }
}
