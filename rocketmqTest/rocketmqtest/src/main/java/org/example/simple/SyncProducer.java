package org.example.simple;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;

/**
 * Hello world!
 *
 */
public class SyncProducer
{
    public static void main( String[] args ) throws MQClientException, UnsupportedEncodingException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("producer_group1");
        producer.setNamesrvAddr("192.168.231.128:9876");
        producer.setInstanceName("producer");
        //失败重试次数
        producer.setRetryTimesWhenSendAsyncFailed(0);
        producer.start();
        Message message = new Message("TopicTest","TagA","hello".getBytes(RemotingHelper.DEFAULT_CHARSET));
        //同步推送消息，会等待发送结果后返回
        SendResult sendResult = producer.send(message);
        System.out.println(sendResult);
        producer.shutdown();
    }
}
