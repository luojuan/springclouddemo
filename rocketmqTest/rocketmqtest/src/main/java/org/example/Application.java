package org.example;

import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

@SpringBootApplication
public class Application implements CommandLineRunner {
    @Autowired
    RocketMQTemplate rocketMQTemplate;

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        testTrainsaction();
    }

    private void testTrainsaction() {
        Message message = MessageBuilder.withPayload("Hello").build();
        SendResult sendResult = rocketMQTemplate.sendMessageInTransaction("myTransactionGroup","TopicA",message,null);
        System.out.println(sendResult);
    }
}
