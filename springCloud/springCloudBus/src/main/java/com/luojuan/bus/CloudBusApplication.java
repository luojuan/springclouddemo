package com.luojuan.bus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CloudBusApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudBusApplication.class, args);
    }

}
