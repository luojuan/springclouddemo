package com.luojuan.app;

import com.luojuan.app.service.MqProviderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class test1 {
    @Autowired
    private MqProviderService mqProviderService;

    @Test
    public void test(){
        mqProviderService.sendMsg("","",2);
    }

    @Test
    public void testDirect(){
        mqProviderService.sendMsgDirect();
    }
    @Test
    public void testTopic(){
        mqProviderService.sendMsgTopic();
    }

    @Test
    public void testTTL(){
        mqProviderService.sendMsgTTL();
    }

    @Test
    public void testMessageTTL(){
        mqProviderService.sendMsgMessgaeTTL();
    }
}
