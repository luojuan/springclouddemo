package com.luojuan.app.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfigration {

    //1.声明注册fanout模式的交换机
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("fanout_order_exchange",true,false);
    }

    //2.声明队列
    @Bean
    public Queue orderQueue(){
        return new Queue("order.fanout.queue",true);
    }


    //3.完成绑定关系
    @Bean
    public Binding orderBind(){
        return BindingBuilder.bind(orderQueue()).to(fanoutExchange());
    }

}
