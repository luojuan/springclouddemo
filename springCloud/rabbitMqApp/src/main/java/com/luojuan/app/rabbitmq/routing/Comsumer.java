package com.luojuan.app.rabbitmq.routing;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Comsumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        new Thread(runnable,"group1").start();
        new Thread(runnable,"group2").start();
        new Thread(runnable,"group3").start();
    }


    private static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //所有的中间件都是基于TCP/IP协议的基础上构建新型的协议规范，只不过rabbitmq遵循的是amqp协议
            // ip port

            //1.创建连接工厂
            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.setHost("127.0.0.1");
            connectionFactory.setPort(5672);
            connectionFactory.setUsername("guest");
            connectionFactory.setPassword("guest");
            connectionFactory.setVirtualHost("/");
            Connection connection = null;
            Channel channel = null;
            final String queueName = Thread.currentThread().getName();
            try {
                //2.创建连接
                connection = connectionFactory.newConnection();
                //3.通过连接获取通道channle
                channel = connection.createChannel();
                Channel finalChannel = channel;
                finalChannel.basicConsume(queueName, true, new DeliverCallback() {
                    @Override
                    public void handle(String consumerTag, Delivery message) throws IOException {
                        System.out.println("收到消息是" + new String(message.getBody(), "UTF-8"));
                    }
                }, new CancelCallback() {
                    @Override
                    public void handle(String s) throws IOException {
                        System.out.println("接收消息失败");
                    }
                });
                System.out.println("开始接收消息");
                System.in.read();
            } catch (Exception e) {

            } finally {
                if(channel!=null&&channel.isOpen()){
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }
                }

                if(connection!=null&&connection.isOpen()){
                    try {
                        connection.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

}
