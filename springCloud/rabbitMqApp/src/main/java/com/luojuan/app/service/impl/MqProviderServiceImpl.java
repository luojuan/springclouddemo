package com.luojuan.app.service.impl;

import com.luojuan.app.service.MqProviderService;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MqProviderServiceImpl implements MqProviderService {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Override
    public void sendMsg(String userId,String productId,int num) {
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生成成功"+orderId);
        //交换机，路由key/queue，消息内容
        String exchangeName = "fanout_order_exchange";
        String routingKey = "";
        rabbitTemplate.convertAndSend(exchangeName,routingKey,orderId);
    }

    @Override
    public void sendMsgDirect() {
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生成成功"+orderId);
        //交换机，路由key/queue，消息内容
        String exchangeName = "direct_order_exchange";
        rabbitTemplate.convertAndSend(exchangeName,"order",orderId);
    }


    @Override
    public void sendMsgTopic() {
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生成成功"+orderId);
        //交换机，路由key/queue，消息内容
        String exchangeName = "topic_order_exchange";
        String routingKey = "a.order.a";
        rabbitTemplate.convertAndSend(exchangeName,routingKey,orderId);
    }

    @Override
    public void sendMsgTTL() {
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生成成功"+orderId);
        //交换机，路由key/queue，消息内容
        String exchangeName = "ttl_direct_exchange";
        rabbitTemplate.convertAndSend(exchangeName,"ttl",orderId);
    }

    @Override
    public void sendMsgMessgaeTTL() {
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生成成功"+orderId);
        //交换机，路由key/queue，消息内容
        String exchangeName = "ttl_order_exchange";

        //给消息设置过期时间
        MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setExpiration("5000");
                message.getMessageProperties().setContentEncoding("UTF-8");
                return message;
            }
        };

        rabbitTemplate.convertAndSend(exchangeName,"ttlMessage",orderId,messagePostProcessor);
    }
}
