package com.luojuan.app.service.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@RabbitListener(queues = {"order.fanout.queue"})
@Service
public class FanOutOrderConsumer {

    @RabbitHandler
    public void reviceMessage(String message){
        System.out.println("sms接收到了消息"+message);
    }

}
