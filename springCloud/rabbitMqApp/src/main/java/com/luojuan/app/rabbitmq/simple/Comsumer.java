package com.luojuan.app.rabbitmq.simple;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Comsumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //所有的中间件都是基于TCP/IP协议的基础上构建新型的协议规范，只不过rabbitmq遵循的是amqp协议
        // ip port

            //1.创建连接工厂
            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.setHost("127.0.0.1");
            connectionFactory.setPort(5672);
            connectionFactory.setUsername("guest");
            connectionFactory.setPassword("guest");
            connectionFactory.setVirtualHost("/");
            //2.创建连接
            Connection connection = connectionFactory.newConnection();
            //3.通过连接获取通道channle
            Channel channel = connection.createChannel();
        try {
            channel.basicConsume("queue1", true, new DeliverCallback() {
                @Override
                public void handle(String consumerTag, Delivery message) throws IOException {
                    System.out.println("收到消息是" + new String(message.getBody(), "UTF-8"));
                }
            }, new CancelCallback() {
                @Override
                public void handle(String s) throws IOException {
                    System.out.println("接收消息失败");
                }
            });
            System.out.println("开始接收消息");
            System.in.read();
        }catch (Exception e){

        }finally {
            channel.close();
            //8.关闭连接
            connection.close();
            System.out.println("aaa");
        }



    }
}
