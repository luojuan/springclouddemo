package com.luojuan.app.controller;

import com.luojuan.app.service.MqProviderService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MqController {
    @Autowired
    MqProviderService mqProviderService;


    public void sendMsg(){
        mqProviderService.sendMsg("aa","aa",2);
    }

}
