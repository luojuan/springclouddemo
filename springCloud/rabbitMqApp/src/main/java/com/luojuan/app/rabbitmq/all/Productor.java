package com.luojuan.app.rabbitmq.all;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Productor {
    public static void main(String[] args) throws IOException, TimeoutException {
        //所有的中间件都是基于TCP/IP协议的基础上构建新型的协议规范，只不过rabbitmq遵循的是amqp协议
        // ip port
        //1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        //2.创建连接
        Connection connection = connectionFactory.newConnection();
        //3.通过连接获取通道channle
        Channel channel = connection.createChannel();
        //4.交换机
        String exchangeName = "direct_message_exchange";
        //5.交换机类型 direct/topic/fanout/headers
        String exchangeType = "direct";

        //交换机名称，交换机类型，是否持久化
        channel.exchangeDeclare(exchangeName,exchangeType,true);
        //队列名称，是否持久化，是否排他，是否自动删除，headers参数
        channel.queueDeclare("queue4",true,false,false,null);
        //绑定队列
        String routingKey = "course";
        //队列名称，交换机名称，routkey
        channel.queueBind("queue4",exchangeName,routingKey);

        String message = "hello";
        //6.发送消息给队列queue
        channel.basicPublish(exchangeName,routingKey,null,message.getBytes());
        //7.关闭通道
        channel.close();
        //8.关闭连接
        connection.close();
    }
}
