package com.luojuan.app.service;

import org.springframework.amqp.core.Message;

public interface MqProviderService {

    public void sendMsg(String userId,String productId,int num);

    public void sendMsgDirect();

    public void sendMsgTopic();

    public void sendMsgTTL();

    public void sendMsgMessgaeTTL();

}
