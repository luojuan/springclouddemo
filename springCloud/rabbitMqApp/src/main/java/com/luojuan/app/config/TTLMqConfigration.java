package com.luojuan.app.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class TTLMqConfigration {
    //1.声明注册fanout模式的交换机
    @Bean
    public DirectExchange ttlDirectExchange(){
        return new DirectExchange("ttl_direct_exchange",true,false);
    }

    //2.声明队列
    @Bean
    public Queue tllDirectQueue(){
        //设置过期时间
        Map<String,Object> map =new HashMap<>();
        map.put("x-message-ttl",5000);
        map.put("x-dead-letter-exchange","dead_direct_exchange");
        map.put("x-dead-letter-routing-key","dead");
        return new Queue("ttl.direct.queue",true,false,false,map);
    }

    //2.声明队列
    @Bean
    public Queue tllMessgaeDirectQueue(){
        return new Queue("ttl.message.direct.queue",true);
    }

    //3.完成绑定关系
    @Bean
    public Binding ttlDirectBind(){
        return BindingBuilder.bind(tllDirectQueue()).to(ttlDirectExchange()).with("ttl");
    }

    //3.完成绑定关系
    @Bean
    public Binding ttlMessageDirectBind(){
        return BindingBuilder.bind(tllMessgaeDirectQueue()).to(ttlDirectExchange()).with("ttlMessage");
    }
}
