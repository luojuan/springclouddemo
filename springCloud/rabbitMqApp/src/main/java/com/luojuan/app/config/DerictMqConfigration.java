package com.luojuan.app.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DerictMqConfigration {
    //1.声明注册fanout模式的交换机
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("direct_order_exchange",true,false);
    }

    //2.声明队列
    @Bean
    public Queue orderDirectQueue(){
        return new Queue("order.direct.queue",true);
    }

    //2.声明队列
    @Bean
    public Queue phoneQueue(){
        return new Queue("phone.direct.queue",true);
    }


    //3.完成绑定关系
    @Bean
    public Binding orderDirectBind(){
        return BindingBuilder.bind(orderDirectQueue()).to(directExchange()).with("order");
    }
    //3.完成绑定关系
    @Bean
    public Binding phoneBind(){
        return BindingBuilder.bind(orderDirectQueue()).to(directExchange()).with("phone");
    }
}
