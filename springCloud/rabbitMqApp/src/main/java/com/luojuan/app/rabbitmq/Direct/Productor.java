package com.luojuan.app.rabbitmq.Direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Productor {
    public static void main(String[] args) throws IOException, TimeoutException {
        //所有的中间件都是基于TCP/IP协议的基础上构建新型的协议规范，只不过rabbitmq遵循的是amqp协议
        // ip port
        //1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        //2.创建连接
        Connection connection = connectionFactory.newConnection();
        //3.通过连接获取通道channle
        Channel channel = connection.createChannel();
        //4.通过连接创建交换机，声明队列，绑定关系，路由key，发送消息，和接收消息

        //5.准备消息内容
        String message = "hello";
        //准备交换机
        String exchangeName = "direct-exchange";
        String routingKey = "email";
        String type = "direct";

        /*
         * @Param1教会员及
         * @Param2队列名称
         * @Param3熟悉配置
         * @Prarm4消息内容
         */
        //6.发送消息给队列queue
        channel.basicPublish(exchangeName,routingKey,null,message.getBytes());
        //7.关闭通道
        channel.close();
        //8.关闭连接
        connection.close();
    }
}
