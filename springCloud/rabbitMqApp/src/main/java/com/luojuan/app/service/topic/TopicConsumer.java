package com.luojuan.app.service.topic;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(bindings = @QueueBinding(value = @Queue(value = "order.topic.queue"),exchange = @Exchange(value = "topic_order_exchange",type = ExchangeTypes.TOPIC),key = "#.order.#"))
public class TopicConsumer {
    @RabbitHandler
    public void reviceMessage(String message){
        System.out.println("sms接收到了消息"+message);
    }
}
