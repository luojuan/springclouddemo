package com.luojuan.app.rabbitmq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Productor {
    public static void main(String[] args) throws IOException, TimeoutException {
        //所有的中间件都是基于TCP/IP协议的基础上构建新型的协议规范，只不过rabbitmq遵循的是amqp协议
        // ip port
        //1.创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        //2.创建连接
        Connection connection = connectionFactory.newConnection();
        //3.通过连接获取通道channle
        Channel channel = connection.createChannel();
        //4.通过连接创建交换机，声明队列，绑定关系，路由key，发送消息，和接收消息
        String queueName = "queue1";
        /*
         * @Param1队列名称
         * @Param2是否要持久化 durable = false 如果false 非持久化 会存盘么
         * @Param3排他性，是否独占独立
         * @Prarm4是否自动删除，随着最后一个消费者消息完毕消息后队列自动删除
         * @Prarm5携带附属参数
         */
        channel.queueDeclare(queueName,false,false,false,null);
        //5.准备消息内容
        String message = "hello";
        //6.发送消息给队列queue
        channel.basicPublish("",queueName,null,message.getBytes());
        //7.关闭通道
        channel.close();
        //8.关闭连接
        connection.close();
    }
}
