package com.luojuan.cloudstream.service;

public interface IMessageProvider {
    public String send();
}
