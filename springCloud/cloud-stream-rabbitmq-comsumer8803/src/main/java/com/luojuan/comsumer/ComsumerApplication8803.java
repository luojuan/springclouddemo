package com.luojuan.comsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ComsumerApplication8803 {

    public static void main(String[] args) {
        SpringApplication.run(ComsumerApplication8803.class, args);
    }

}
