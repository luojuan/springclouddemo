package com.luojuan.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/appClient")
public class AppClientController {

    @Autowired
    RestTemplate restTemplate;

    private static final String APP_SERVER_URL =  "http://APP-SERVICE";

    @GetMapping("/getApp")
    public String getApp(){
        return restTemplate.getForObject(APP_SERVER_URL+"/appServer/getApp",String.class);
    }


    @GetMapping("/getApp2")
    public ResponseEntity<String> getApp2(){
        ResponseEntity<String> entity = restTemplate.getForEntity(APP_SERVER_URL+"/appServer/getApp",String.class);
        if(entity.getStatusCode().is2xxSuccessful()) {
            return entity;
        }else{
            return null;
        }
//        return restTemplate.getForObject(APP_SERVER_URL+"/appServer/getApp",String.class);
    }

}
