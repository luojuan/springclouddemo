package com.luojuan.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/appServer")
public class AppServerController {

    @GetMapping("/getApp")
    public String getApp(){
        return "hello 8002";
    }

}
